(* Board *)

type board = Map.map list

(* init board *)
let initBoard () =
    let rec loop i acc =
        match i with
            | 9 -> acc
            | _ -> loop (succ i) (acc @ ((Map.initMap i) :: []))
    in
        loop 0 []

let printBoard board =
    let rec loop board i =
        let x = i mod 3 in
        let y = i / 9
        in
        print_string (Map.rowToString (List.nth board (x + y * 3)) ((i mod 9) / 3));
        match i with
        | 8 -> print_endline "\n---------------------"; loop board (succ i)
        | 17 -> print_endline "\n---------------------"; loop board (succ i)
        | 26 -> print_endline ""
        | _ when (i mod 3) = 2 -> print_endline ""; loop board (succ i)
        | _ -> print_string " | "; loop board (succ i)
    in
    loop board 0

let stroke f board pos ply=
    let checkMap map ply =
        if (Check.checkMapFinished map) = true  && (Map.isFinish map = false) then
            (Map.finishMap map ply)
        else map
    in
    let doStroke f map pos =
        let x = (Map.getIndex map)
        in
        match pos with
        | (a, b) -> (
            if x = a then checkMap (Map.stroke f map b) ply
            else checkMap (map) ply
        )
    in
    let rec loop f board pos acc =
        match board with
        | [] -> acc
        | head :: tail -> loop f tail pos (acc @ (doStroke f head pos :: []))
    in
    loop f board pos []


