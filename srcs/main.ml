(* main.ml *)

let winMsg ply = ply ^ " HAS WIN"

let rec mainLoop board ply =
    let acc = Game.playTurn board ply
    in
    Board.printBoard acc;
    print_endline "";
    match (Check.checkBoardFinished acc) with
    | false -> mainLoop acc (Game.nextTurn ply)
    | true -> (
        Board.printBoard acc;
        if Check.equality acc = true then "EQUALITY"
        else winMsg ply
    )


let () =
    let board = (Board.initBoard ())
    in
    Board.printBoard board;
    print_endline (mainLoop board "O")

